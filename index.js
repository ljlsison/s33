//3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos',{method: "GET"})
.then(response => response.json())
.then(data => console.log(data))

//4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
fetch("https://jsonplaceholder.typicode.com/todos",{method: "GET"})
.then(response => response.json())
.then(data => {console.log(data.map(post => post.title))});

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((data) => console.log(data))

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
fetch("https://jsonplaceholder.typicode.com/todos/1",{method: "GET"})
.then(response => response.json())
.then(response => console.log(response))

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/posts',{
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		userId: 2,
		id: 3,
		title: "sample only",
		completed: false
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 8. Create a fetch request using the PUT method that will update a to dolist item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/posts/101',{
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Corrected Post',
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

/*
Update a to do list item by changing the data structure to contain
the following properties:
a. Title
b. Description
c. Status
d. Date Completed
e. User ID
*/

fetch('https://jsonplaceholder.typicode.com/posts/101',{
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Corrected Post',
		description: 'magnum opus',
		status: 'completed',
		date_completed: '3 October 2022',
		userID: 3,
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 10. Create a fetch request using the PATCH method that will update a todo list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Patched Post',
		completed: true,
		userID: 3,
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
fetch('https://jsonplaceholder.typicode.com/posts/0',{
	method: 'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title:'Patched Post',
		completed: true,
		date_completed: "3 October 2022",
		userID: 3,
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// Create a fetch request using the DELETE method that will delete anitem using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'})
